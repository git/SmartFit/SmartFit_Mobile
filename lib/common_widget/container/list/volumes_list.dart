import 'package:flutter/material.dart';
import 'package:smartfit_app_mobile/common_widget/container/container_stats_activities.dart';
import 'package:smartfit_app_mobile/modele/convertisseur.dart';

class VolumesList extends StatelessWidget {
  final Map<String, dynamic> volume;

  const VolumesList({super.key, required this.volume});
  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context).size;

    // TODO: True message with variables and context aware
    if (volume["nbActivity"] == 0) {
      return const Text("No activity the last x days/month/year");
    }

    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ContainerStatsActivities(volume["nbActivity"].toString(),
              "Number of activities", Icons.numbers),
          SizedBox(
            width: media.width * 0.03,
          ),
          ContainerStatsActivities(
              "${Convertisseur.secondeIntoMinute(volume["durationActiviy"]).toStringAsFixed(0)} min",
              "Total time",
              Icons.timer),
          SizedBox(
            width: media.width * 0.03,
          ),
          ContainerStatsActivities(
              volume["bpmAvg"].toString(), "Average bpm", Icons.favorite),
          SizedBox(
            width: media.width * 0.03,
          ),
          ContainerStatsActivities(
              " ${Convertisseur.msIntoKmh(volume["speedAvg"]).toStringAsFixed(2)} km/h",
              "Average speed",
              Icons.bolt),
          SizedBox(
            width: media.width * 0.03,
          ),
          ContainerStatsActivities(
              "${volume["denivelePositif"].toStringAsFixed(2)} m",
              "Positive height difference",
              Icons.hiking),
        ],
      ),
    );
  }
}
